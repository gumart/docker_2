FROM ubuntu:18.04 as build

ENV TZ=Asia/Krasnoyarsk

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update && apt -y install curl software-properties-common git unzip\
    && add-apt-repository ppa:ondrej/php \
    && apt -y install php8.1 php8.1-curl php8.1-dom php8.1-zip \
    && curl -sS https://getcomposer.org/installer -o composer-setup.php \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && rm composer-setup.php

COPY . /var/www

WORKDIR /var/www

RUN composer update

FROM ubuntu:18.04

COPY --from=build /etc/localtime /etc/localtime
COPY --from=build /etc/timezone /etc/timezone
COPY --from=build --chown=www-data:www-data /var/www /var/www

RUN apt update && apt -y install nginx software-properties-common \
    && add-apt-repository ppa:ondrej/php \
    # && apt -y install mysql-server \
    && apt -y install php8.1-fpm php8.1-mysql php8.1 php8.1-curl php8.1-dom php8.1-zip

WORKDIR /var/www

VOLUME [/var/lib/mysql]

COPY nginx.conf /etc/nginx
COPY entrypoint.sh /var/

ENTRYPOINT sh /var/entrypoint.sh
