#!/bin/sh

apt -y install mysql-server

/etc/init.d/mysql start

mysql -e "CREATE DATABASE IF NOT EXISTS \`$DB_DATABASE\` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
mysql -e "GRANT ALL ON \`$DB_DATABASE\`.* to '$DB_USERNAME'@'%' IDENTIFIED BY '$DB_PASSWORD';"
mysql -e "DROP USER IF EXISTS ''@'localhost';"
mysql -e "DROP DATABASE IF EXISTS test;"
mysql -e "FLUSH PRIVILEGES;"

php artisan migrate

/etc/init.d/php8.1-fpm start

nginx -g "daemon off;"
